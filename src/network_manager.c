#include "network_manager.h"

#include <assert.h>
#include <Windows.h>
#include <time.h>

#include "logger.h"
#include "game_packets.h"

static sfClock* loopClock = NULL;

static bool isRunning = false;

static ClientNetworkData clientData;
static HostNetworkData hostData;

static HostSetupInfo hostSetupInfo;
static ClientSetupInfo clientSetupInfo;
static HostConnectedClient connectedClient;

bool start_host(HostSetupInfo setup)
{
	memcpy_s(&hostSetupInfo, sizeof(HostSetupInfo), &setup, sizeof(HostSetupInfo));

	hostData.thread = sfThread_create(run_host, NULL);
	assert(hostData.thread);
	if (!hostData.thread)
	{
		log_err("Unable to create host thread");
		return false;
	}

	hostData.mutex = sfMutex_create();
	assert(hostData.mutex);
	if (!hostData.mutex)
	{
		log_err("Unable to create mutex for host");
		return false;
	}


	loopClock = sfClock_create();
	assert(loopClock);
	if (!loopClock)
	{
		log_err("Unable to create clock for host");
		return false;
	}

	hostData.listener = sfTcpListener_create();
	assert(hostData.listener);
	if (!hostData.listener)
	{
		log_err("Unable to create tcp listener for host");
		return false;
	}

	/*hostTcpSocket = sfTcpSocket_create();
	assert(hostTcpSocket);
	if (!hostTcpSocket)
	{
		log_err("Unable to create tcp socket for host");
		return false;
	}*/
	hostData.packet = sfPacket_create();
	assert(hostData.packet);
	if (!hostData.packet)
	{
		log_err("Unable to create host packet");
		return false;
	}

	sfThread_launch(hostData.thread);
	isRunning = true;

	return true;
}

void terminate_host()
{
	sfThread_terminate(hostData.thread);
	sfThread_destroy(hostData.thread);

	sfMutex_destroy(hostData.mutex);
	sfClock_destroy(loopClock);
	sfTcpListener_destroy(hostData.listener);

	sfTcpSocket_disconnect(hostData.socket);
	sfTcpSocket_destroy(hostData.socket);
	sfPacket_destroy(hostData.packet);

	hostData.mutex = NULL;
	hostData.thread = NULL;
	loopClock = NULL;
	hostData.packet = NULL;
	hostData.listener = NULL;
	hostData.socket = NULL;
	isRunning = false;
}

bool is_host_running()
{
	return isRunning;
}

bool start_client(ClientSetupInfo csi)
{
	memcpy_s(&clientSetupInfo, sizeof(ClientSetupInfo), &csi, sizeof(ClientSetupInfo));

	clientData.thread = sfThread_create(run_client, NULL);
	assert(clientData.thread);
	if (!clientData.thread)
	{
		log_err("Unable to create client thread");
		return false;
	}

	clientData.mutex = sfMutex_create();
	assert(clientData.mutex);
	if (!clientData.mutex)
	{
		log_err("Unable to create mutex for client");
		return false;
	}

	loopClock = sfClock_create();
	assert(loopClock);
	if (!loopClock)
	{
		log_err("Unable to create clock for client");
		return false;
	}

	clientData.socket = sfTcpSocket_create();
	assert(clientData.socket);
	if (!clientData.socket)
	{
		log_err("Unable to create client tcp socket");
		return false;
	}

	clientData.packet = sfPacket_create();
	assert(clientData.packet);
	if (!clientData.packet)
	{
		log_err("Unable to create client packet");
		return false;
	}

	sfThread_launch(clientData.thread);
	isRunning = true;

	return true;
}

void terminate_client()
{
	sfThread_terminate(clientData.thread);
	sfThread_destroy(clientData.thread);

	sfTcpSocket_destroy(clientData.socket);
	sfMutex_destroy(clientData.mutex);
	sfClock_destroy(loopClock);

	sfPacket_destroy(clientData.packet);

	clientData.packet = NULL;
	clientData.thread = NULL;
	clientData.socket = NULL;
	loopClock = NULL;
	clientData.mutex = NULL;
	isRunning = false;
}

bool is_client_running()
{
	return isRunning;
}

void run_host(void* data)
{
	sfClock_restart(loopClock);
	//sfTcpListener_setBlocking(hostData.listener, false);
	NetworkGameStage networkStage = ngsEstablish;

	while (true)
	{
		sfTime t = sfClock_restart(loopClock);
		int32 ms = sfTime_asMilliseconds(t);
		//log_dbg("Testing host thread %d", ms);
		switch (networkStage)
		{
		case ngsEstablish:
			if (wait_for_client_connection())
			{
				networkStage = ngsGameSetup;
				log_dbg("Client connected");
			}
			break;
		case ngsGameSetup: 
			break;
		default:
			assert(false);
			break;
		}

		if (ms < 16)
		{
			Sleep(16 - ms);
		}
	}
}

bool wait_for_client_connection()
{
	sfSocketStatus socketStatus = sfTcpListener_listen(hostData.listener, hostSetupInfo.tcpPort, sfIpAddress_Any);
	if (socketStatus != sfSocketDone)
	{
		log_trace("No client connecting");
		return false;
	}

	socketStatus = sfTcpListener_accept(hostData.listener, &hostData.socket);
	if (socketStatus != sfSocketDone)
	{
		//log_trace("Unable to accept client");
		return false;
	}

	assert(hostData.socket);
	if (!hostData.socket)
	{
		log_err("Null host tcp socket");
		return false;
	}

	//sfTcpSocket_setBlocking(hostData.socket, false);

	connectedClient.ipAddr = sfTcpSocket_getRemoteAddress(hostData.socket);
	connectedClient.port = sfTcpSocket_getRemotePort(hostData.socket);
	connectedClient.lastContactTime = (uint64)(time(NULL));
	return true;
}

void run_client(void* data)
{
	sfClock_restart(loopClock);
	NetworkGameStage networkStage = ngsEstablish;

	//sfTcpSocket_setBlocking(clientData.socket, false);

	while (true)
	{
		sfTime t = sfClock_restart(loopClock);
		int32 ms = sfTime_asMilliseconds(t);
		//log_dbg("Client connection step");

		switch (networkStage)
		{
		case ngsEstablish:

			if (establish_connection_to_host())
			{
				networkStage = ngsGameSetup;
			}
			break;
		case ngsGameSetup:
			if (wait_for_game_setup())
			{
				networkStage = ngsGameSetup;
			}
			break;
		default:
			assert(false);
			break;
		}


		if (ms < 16)
		{
			Sleep(16 - ms);
		}
	}
}

bool establish_connection_to_host()
{
	sfSocketStatus ss = sfTcpSocket_connect(clientData.socket, sfIpAddress_LocalHost, clientSetupInfo.tcpPort, sfSeconds(1));
	if (ss != sfSocketDone)
	{
		return  false;
	}

	log_trace("Client connected to host");
	return true;
}

bool wait_for_game_setup()
{
	sfPacket_clear(clientData.packet);
	sfSocketStatus ss = sfTcpSocket_receivePacket(clientData.socket, clientData.packet);
	if (ss != sfSocketDone)
	{
		return false;
	}

	//TODO Unpack game setup packet

	return true;
}

bool send_packet(sfTcpSocket* socket, sfPacket* packet)
{
	sfSocketStatus ss = sfSocketDone;
	do
	{
		ss = sfTcpSocket_sendPacket(socket, packet);
		if (ss == sfSocketPartial)
		{
			continue;
		}
		else
		{
			return false;
		}
	} while (ss != sfSocketDone);

	return true;
}
