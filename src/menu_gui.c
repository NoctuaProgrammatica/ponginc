#include "menu_gui.h"
#include <assert.h>

#include "logger.h"
#include "types.h"
#include "game.h"
#include "application.h"

#include "network_manager.h"

static sfText* menuTitleText = NULL;
static sfText* menuPlayButton = NULL;
static sfText* menuCreditsButton = NULL;

static sfText* playButtonSingleplayer = NULL;
static sfText* playButtonMultiplayer = NULL;

static sfText* hostGameButton = NULL;
static sfText* joinGameButton = NULL;
static sfText* hostPortTextEntry = NULL;
static sfText* joinGameIPTextEntry = NULL;

static MenuState activeMenuState = mMainOptions;
const static sfColor ButtonBaseCol = { 255,255,0,255 };

static bool isLeftClickPressed = false;

bool setup_menu_state(sfFont* font)
{
	assert(font);
	if (!font)
	{
		log_err("Unable to create menu due to null font");
		return false;
	}

	const sfView* v = sfRenderWindow_getView(renderWindow);
	const vec2f ViewSize = { (float)(VIEW_WIDTH), (float)(VIEW_HEIGHT) };

	// Title Text
	menuTitleText = sfText_create();
	assert(menuTitleText);
	if (!menuTitleText)
	{
		log_err("Failed to create menu title text");
		return false;
	}

	sfText_setFont(menuTitleText, font);
	sfText_setString(menuTitleText, "SPEED-PONG!");
	sfText_setFillColor(menuTitleText, ButtonBaseCol);
	sfText_setStyle(menuTitleText, sfTextBold);
	sfText_setCharacterSize(menuTitleText, 72);
	vec2f titleTextPos = { ViewSize.x / 2.0f, 10.0f };
	titleTextPos.x -= (sfText_getGlobalBounds(menuTitleText).width / 2.0f);
	sfText_setPosition(menuTitleText, titleTextPos);

	// Play Button
	menuPlayButton = sfText_create();
	assert(menuPlayButton);
	if (!menuPlayButton)
	{
		log_err("Failed to create menu play button");
		return false;
	}

	sfText_setFont(menuPlayButton, font);
	sfText_setString(menuPlayButton, "PLAY");
	sfText_setFillColor(menuPlayButton, ButtonBaseCol);
	sfText_setStyle(menuPlayButton, sfTextBold);
	sfText_setCharacterSize(menuPlayButton, 48);

	vec2f tempPos = multiply_vec2f_float(&ViewSize, 0.5f);
	vec2f playSize = { sfText_getGlobalBounds(menuPlayButton).width,sfText_getGlobalBounds(menuPlayButton).height };
	vec2f halfSize = multiply_vec2f_float(&playSize, 0.5f);
	tempPos = sub_vec2f(&tempPos, &halfSize);
	tempPos.y -= 64.0f;
	sfText_setPosition(menuPlayButton, tempPos);


	// Credits Button
	menuCreditsButton = sfText_create();
	assert(menuCreditsButton);

	if (!menuCreditsButton)
	{
		log_err("Failed to create menu credits button");
		return false;
	}

	sfText_setFont(menuCreditsButton, font);
	sfText_setString(menuCreditsButton, "CREDITS");
	sfText_setFillColor(menuCreditsButton, ButtonBaseCol);
	sfText_setStyle(menuCreditsButton, sfTextBold);
	sfText_setCharacterSize(menuCreditsButton, 48);
	tempPos.y += 105.0f;
	tempPos.x = ViewSize.x / 2.0f - sfText_getGlobalBounds(menuCreditsButton).width / 2.0f;
	sfText_setPosition(menuCreditsButton, tempPos);


	// Play Single player button options 
	playButtonSingleplayer = sfText_create();
	assert(playButtonSingleplayer);
	if (!playButtonSingleplayer)
	{
		log_err("Failed to create singleplayer button");
		return false;
	}

	sfText_setFont(playButtonSingleplayer, font);
	sfText_setString(playButtonSingleplayer, "Player vs AI");
	sfText_setFillColor(playButtonSingleplayer, sfYellow);
	sfText_setCharacterSize(playButtonSingleplayer, 42);
	tempPos = multiply_vec2f_float(&ViewSize, 0.5f);
	tempPos.x -= sfText_getGlobalBounds(playButtonSingleplayer).width / 2.0f;
	tempPos.y -= 24.0f;
	sfText_setPosition(playButtonSingleplayer, tempPos);

	// Play multiplayer button options 
	playButtonMultiplayer = sfText_create();
	assert(playButtonMultiplayer);
	if (!playButtonMultiplayer)
	{
		log_err("Failed to create multiplayer button");
		return false;
	}

	sfText_setFont(playButtonMultiplayer, font);
	sfText_setString(playButtonMultiplayer, "Player vs Player");
	sfText_setFillColor(playButtonMultiplayer, sfYellow);
	sfText_setCharacterSize(playButtonMultiplayer, 42);
	tempPos.x = (ViewSize.x / 2.0f) - sfText_getGlobalBounds(playButtonMultiplayer).width / 2.0f;
	tempPos.y += sfText_getGlobalBounds(playButtonMultiplayer).height + 28.0f;
	sfText_setPosition(playButtonMultiplayer, tempPos);

	// Host game options
	hostGameButton = sfText_create();
	assert(hostGameButton);
	if (!hostGameButton)
	{
		log_err("Failed to create singleplayer button");
		return false;
	}

	sfText_setFont(hostGameButton, font);
	sfText_setString(hostGameButton, "Host Game");
	sfText_setFillColor(hostGameButton, sfYellow);
	sfText_setCharacterSize(hostGameButton, 42);
	tempPos = multiply_vec2f_float(&ViewSize, 0.5f);
	tempPos.x -= sfText_getGlobalBounds(hostGameButton).width / 2.0f;
	tempPos.y -= 24.0f;
	sfText_setPosition(hostGameButton, tempPos);

	// Join button options 
	joinGameButton = sfText_create();
	assert(joinGameButton);
	if (!joinGameButton)
	{
		log_err("Failed to create multiplayer button");
		return false;
	}

	sfText_setFont(joinGameButton, font);
	sfText_setString(joinGameButton, "Join Game");
	sfText_setFillColor(joinGameButton, sfYellow);
	sfText_setCharacterSize(joinGameButton, 42);
	tempPos.x = (ViewSize.x / 2.0f) - sfText_getGlobalBounds(joinGameButton).width / 2.0f;
	tempPos.y += sfText_getGlobalBounds(joinGameButton).height + 28.0f;
	sfText_setPosition(joinGameButton, tempPos);


	return true;
}

void update_menu_state(float dt, void (*reset_ptr) ())
{
	const vec2i MouseScreenPos = sfMouse_getPositionRenderWindow(renderWindow);
	const vec2f MouseWorldPos = sfRenderWindow_mapPixelToCoords(renderWindow, MouseScreenPos, NULL);
	switch (activeMenuState)
	{
	default:
	case mMainOptions:
		update_main_options(dt, &MouseWorldPos);
		break;
	case mPlayOptions:
		update_play_options(dt, &MouseWorldPos, reset_ptr);
		break;
	case mMultiplayerOptions:
		update_multiplayer_options(dt, &MouseWorldPos);
		break;
	}
}

void handle_menu_events(sfEvent* evt)
{
	if (evt->type == sfEvtMouseButtonPressed)
	{
		if (evt->mouseButton.button == sfMouseLeft)
		{
			isLeftClickPressed = true;
		}
	
	}
}

void draw_menu_state()
{
	sfRenderWindow_drawRectangleShape(renderWindow, playerPaddle.shape, NULL);
	sfRenderWindow_drawRectangleShape(renderWindow, aiPaddle.shape, NULL);
	sfRenderWindow_drawText(renderWindow, menuTitleText, NULL);

	switch (activeMenuState)
	{
	default:
	case mMainOptions:
		sfRenderWindow_drawText(renderWindow, menuPlayButton, NULL);
		sfRenderWindow_drawText(renderWindow, menuCreditsButton, NULL);
		break;
	case mPlayOptions:
		sfRenderWindow_drawText(renderWindow, playButtonSingleplayer, NULL);
		sfRenderWindow_drawText(renderWindow, playButtonMultiplayer, NULL);
		break;
	case mMultiplayerOptions:
		sfRenderWindow_drawText(renderWindow, joinGameButton, NULL);
		sfRenderWindow_drawText(renderWindow, hostGameButton, NULL);
		break;
	case mHostGameOptions:
	case mJoinGameOptions:
		break;

	}
	isLeftClickPressed = false;
}

void cleanup_menu()
{
	sfText_destroy(menuPlayButton);
	sfText_destroy(menuCreditsButton);
	sfText_destroy(menuTitleText);

	sfText_destroy(playButtonSingleplayer);
	sfText_destroy(playButtonMultiplayer);
}

void return_to_menu()
{
	activeMenuState = mMainOptions;
}

void update_main_options(float dt, vec2f* mousePos)
{
	assert(mousePos);
	const sfFloatRect PlayBounds = sfText_getGlobalBounds(menuPlayButton);
	const sfFloatRect CreditsBounds = sfText_getGlobalBounds(menuCreditsButton);

	if (sfFloatRect_contains(&PlayBounds, mousePos->x, mousePos->y))
	{
		sfText_setFillColor(menuPlayButton, get_hover_col(&ButtonBaseCol));

		if (isLeftClickPressed)
		{
			activeMenuState = mPlayOptions;

		}
	}
	else
	{
		sfText_setFillColor(menuPlayButton, ButtonBaseCol);
	}

	if (sfFloatRect_contains(&CreditsBounds, mousePos->x, mousePos->y))
	{
		sfText_setFillColor(menuCreditsButton, get_hover_col(&ButtonBaseCol));
	}
	else
	{
		sfText_setFillColor(menuCreditsButton, ButtonBaseCol);
	}
}

void update_play_options(float dt, vec2f* mousePos, void (*reset_ptr) ())
{
	assert(mousePos);
	const sfFloatRect PvEBounds = sfText_getGlobalBounds(playButtonSingleplayer);
	const sfFloatRect PvPBounds = sfText_getGlobalBounds(playButtonMultiplayer);

	if (sfFloatRect_contains(&PvEBounds, mousePos->x, mousePos->y))
	{
		sfText_setFillColor(playButtonSingleplayer, get_hover_col(&ButtonBaseCol));
		if (isLeftClickPressed)
		{
			appState = sGamePlayAi;
			reset_ptr();
		}
	}
	else
	{
		sfText_setFillColor(playButtonSingleplayer, ButtonBaseCol);
	}

	if (sfFloatRect_contains(&PvPBounds, mousePos->x, mousePos->y))
	{
		sfText_setFillColor(playButtonMultiplayer, get_hover_col(&ButtonBaseCol));
		if (isLeftClickPressed)
		{
			activeMenuState = mMultiplayerOptions;

			/*HostSetupInfo hsi;
			hsi.tcpPort = 53000;
			hsi.udpPort = 53001;
			hsi.disconnectTimer = 30000;
			const bool hostStartResult = start_host(hsi);
			if (!hostStartResult)
			{
				activeMenuState = mMainOptions;
			}
			else
			{
			}*/
		}
	}
	else
	{
		sfText_setFillColor(playButtonMultiplayer, ButtonBaseCol);
	}
}

void update_multiplayer_options(float dt, vec2f* mousePos)
{
	assert(mousePos);
	const sfFloatRect HostBounds = sfText_getGlobalBounds(hostGameButton);
	const sfFloatRect JoinBounds = sfText_getGlobalBounds(joinGameButton);

	if (sfFloatRect_contains(&HostBounds, mousePos->x, mousePos->y))
	{
		sfText_setFillColor(hostGameButton, get_hover_col(&ButtonBaseCol));
		if (isLeftClickPressed)
		{
			HostSetupInfo hsi;
			hsi.tcpPort = 53632;
			hsi.udpPort = 53001;
			hsi.disconnectTimer = 30000;
			const bool hostStartResult = start_host(hsi);
			if (!hostStartResult)
			{
				// Failed to create host
				activeMenuState = mMainOptions;
				terminate_host();
			}
			else
			{
				// Host creation successful
				activeMenuState = mHostGameOptions;
			}
		}
	}
	else
	{
		sfText_setFillColor(hostGameButton, ButtonBaseCol);
	}

	if (sfFloatRect_contains(&JoinBounds, mousePos->x, mousePos->y))
	{
		sfText_setFillColor(joinGameButton, get_hover_col(&ButtonBaseCol));
		if (isLeftClickPressed)
		{
			activeMenuState = mJoinGameOptions;
			ClientSetupInfo csi;
			csi.tcpPort = 53632;
			csi.udpPort = 53001;
			csi.ipAddr = sfIpAddress_LocalHost; //FOR IN-DEV

			const bool joinStartResult = start_client(csi);
			if (!joinStartResult)
			{
				// Failed to create join
				activeMenuState = mMainOptions;
				terminate_client();
			}
			else
			{
				// Client creation successful
				activeMenuState = mJoinGameOptions;
			}
		}
	}
	else
	{
		sfText_setFillColor(joinGameButton, ButtonBaseCol);
	}
}

sfColor get_hover_col(sfColor* col)
{
	return sfColor_fromRGB(col->r / 2, col->g / 2, col->b / 2);
}
