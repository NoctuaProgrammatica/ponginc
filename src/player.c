#include "player.h"
#include "game.h"
#include "vector_utils.h"
#include "math_utils.h"
#include "application.h"

void update_player(float dt)
{
	vec2f move = { 0.0f, 0.0f };

	if (sfKeyboard_isKeyPressed(sfKeyW) || sfKeyboard_isKeyPressed(sfKeyUp))
	{
		move.y = -PADDLE_MOVE_SPEED * dt * speedMultiplier;
	}

	if (sfKeyboard_isKeyPressed(sfKeyS) || sfKeyboard_isKeyPressed(sfKeyDown))
	{
		move.y = PADDLE_MOVE_SPEED * dt * speedMultiplier;
	}

	playerPaddle.position = add_vec2f(&playerPaddle.position, &move);

	const sfView* v = sfRenderWindow_getView(renderWindow);
	const float Height = sfView_getSize(v).y;

	playerPaddle.position.y = clamp_float(PADDLE_HEIGHT / 2.0f, Height - (PADDLE_HEIGHT / 2.0f), playerPaddle.position.y);

	sfRectangleShape_setPosition(playerPaddle.shape, playerPaddle.position);
}
