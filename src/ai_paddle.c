#include "ai_paddle.h"
#include <stdio.h>

#include "game.h"

void update_ai_paddle(float dt)
{
	const sfFloatRect Bounds = sfRectangleShape_getGlobalBounds(aiPaddle.shape);
	const vec2f PaddlePos = sfRectangleShape_getPosition(aiPaddle.shape);

	const float BallRadius = sfCircleShape_getRadius(gameBall.shape);

	vec2f move = { 0.0f, 0.0f };

	// If ball is heading towards AI
	if (gameBall.direction.x > 0.0f)
	{
		// If the ball is above or below the paddle
		if (Bounds.top > gameBall.position.y)
		{
			// Move paddle up
			move.y = -1.0f;
		}
		else if (Bounds.top + Bounds.height < gameBall.position.y)
		{
			// Move Paddle down
			move.y = 1.0f;
		}
	}

	// Move the paddle
	move = multiply_vec2f_float(&move, speedMultiplier * PADDLE_MOVE_SPEED * dt);
	aiPaddle.position = add_vec2f(&aiPaddle.position, &move);
	sfRectangleShape_setPosition(aiPaddle.shape, aiPaddle.position);
}
