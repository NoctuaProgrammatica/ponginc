#include "game.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h> 
#include <time.h>
#include <SFML\Audio.h>

#include "math_utils.h"
#include "application.h"
#include "player.h"
#include "collision_tests.h"
#include "ai_paddle.h"
#include "logger.h"
#include "menu_gui.h"

extern sfRenderWindow* renderWindow;

const float PADDLE_WIDTH = 32.0f;
const float PADDLE_HEIGHT = 128.0f;
const float BALL_RADIUS = 24.0f;
const float PADDLE_MOVE_SPEED = 140.0f;
const float BALL_MOVE_SPEED = 170.0f;
const uint32 MAX_SCORE = 12;

paddle playerPaddle;
paddle aiPaddle;
ball gameBall;

static sfFont* font = NULL;
static sfText* scoreTextPlayer = NULL;
static sfText* scoreTextAi = NULL;

static sfText* winScreenText = NULL;
static sfText* failScreenText = NULL;

static sfSoundBuffer* paddleHitSoundBuffer = NULL;
static sfSound* paddleHitSfx = NULL;

static sfRectangleShape* dividingLine = NULL;
static sfRectangleShape* topBorder = NULL;
static sfRectangleShape* bottomBorder = NULL;
static sfRectangleShape* leftBorder = NULL;
static sfRectangleShape* rightBorder = NULL;

ApplicationState appState = sMenu;

static bool isFirstUpdate = true;
float speedMultiplier = 1.0f;

static const char* AppStateStrings[] =
{
	"Menu",
	"GamePlay",
	"Credits",
	"FailScreen",
	"WinScreen"
};

bool setup_game()
{
	if (!setup_paddles())
	{
		return false;
	}

	if (!setup_border())
	{
		return false;
	}

	if (!setup_ball())
	{
		return false;
	}

	if (!setup_dividing_line())
	{
		return false;
	}

	if (!setup_score_text())
	{
		return false;
	}

	if (!setup_menu_state(font))
	{
		return false;
	}

	if (!setup_win_screen())
	{
		return false;
	}

	if (!setup_fail_screen())
	{
		return false;
	}

	if (!load_sounds())
	{
		return false;
	}

	sfColor clear = sfColor_fromRGB(30, 30, 30);
	set_clear_colour(&clear);

	srand(time(NULL));
	appState = sMenu;

	return true;
}

void handle_event(sfEvent* evt)
{
	assert(evt);
	if (evt->type == sfEvtKeyPressed)
	{
#ifdef _DEBUG
		if (evt->key.code == sfKeyUp)
		{
			--appState;
			if (appState < 0)
			{
				appState = MAX_APP_SATE - 1;
			}

			log_dbg("App State is %s\n", AppStateStrings[appState]);
		}

		if (evt->key.code == sfKeyDown)
		{
			++appState;
			if (appState >= MAX_APP_SATE)
			{
				appState = 0;
			}
			log_dbg("App State is %s\n", AppStateStrings[appState]);
		}

#endif
	}

	if (appState == sMenu)
	{
		handle_menu_events(evt);
	}
}

void update_game(float dt)
{
	switch (appState)
	{
	default:
	case sMenu:
		update_menu_state(dt, reset_game);
		break;
	case sGamePlayAi:
		update_state_gameplay(dt);
		break;
	case sCredits:
		//TODO implement credits screen
		break;
	case sFailScreen:
	case sWinScreen:
		update_state_game_end(dt);
		break;
	}
}

void draw_game()
{
	switch (appState)
	{
	default:
	case sMenu:
		draw_menu_state();
		break;
	case sGamePlayAi:
	case sGameplayNetwork:
		draw_state_gameplay();
		break;
	case sCredits:
		//TODO implement credits screen
		break;
	case sWinScreen:
	case sFailScreen:
		draw_state_game_end();
		break;
	}
}

void cleanup_game()
{
	// Destroy paddles & dividing line
	sfRectangleShape_destroy(playerPaddle.shape);
	sfRectangleShape_destroy(aiPaddle.shape);
	sfRectangleShape_destroy(dividingLine);
	sfRectangleShape_destroy(topBorder);
	sfRectangleShape_destroy(bottomBorder);
	sfRectangleShape_destroy(leftBorder);
	sfRectangleShape_destroy(rightBorder);


	// Destroy gameball 
	sfCircleShape_destroy(gameBall.shape);

	// Destroy text 
	sfText_destroy(scoreTextPlayer);
	sfText_destroy(scoreTextAi);
	sfText_destroy(winScreenText);
	sfText_destroy(failScreenText);
	sfFont_destroy(font);

	// Sound destructions 
	sfSound_destroy(paddleHitSfx);
	sfSoundBuffer_destroy(paddleHitSoundBuffer);
}

bool setup_paddles()
{
	// Allocate each of the paddles and set their identifier
	playerPaddle.shape = sfRectangleShape_create();
	assert(playerPaddle.shape);
	const uint64 IdSize = sizeof(playerPaddle.id) / sizeof(int8);
	int32 error = strcpy_s(playerPaddle.id, IdSize, "Player Paddle");
	if (error)
	{
		log_err("Unable to setup player id string");
		return false;
	}

	aiPaddle.shape = sfRectangleShape_create();
	assert(aiPaddle.shape);
	error = strcpy_s(aiPaddle.id, IdSize, "AI Paddle");
	if (error)
	{
		log_err("Unable to setup ai id string");
		return false;
	}

	// Setup the paddle shapes
	const vec2f PaddleSize = { PADDLE_WIDTH, PADDLE_HEIGHT };
	const vec2u WindowSize = sfRenderWindow_getSize(renderWindow);

	sfRectangleShape_setSize(playerPaddle.shape, PaddleSize);
	sfRectangleShape_setSize(aiPaddle.shape, PaddleSize);

	sfRectangleShape_setOrigin(playerPaddle.shape, multiply_vec2f_float(&PaddleSize, 0.5f));
	sfRectangleShape_setOrigin(aiPaddle.shape, multiply_vec2f_float(&PaddleSize, 0.5f));

	sfRectangleShape_setOutlineThickness(playerPaddle.shape, 2.0f);
	sfRectangleShape_setOutlineThickness(aiPaddle.shape, 2.0f);

	const sfColor PlayerColOutline = sfColor_fromRGB(39, 203, 150);
	const sfColor PlayerColFill = sfColor_fromRGB(38, 152, 29);

	const sfColor AiColOutline = sfColor_fromRGB(221, 142, 55);
	const sfColor AiFillColor = sfColor_fromRGB(209, 62, 35);

	sfRectangleShape_setOutlineColor(playerPaddle.shape, PlayerColOutline);
	sfRectangleShape_setOutlineColor(aiPaddle.shape, AiColOutline);

	sfRectangleShape_setFillColor(playerPaddle.shape, PlayerColFill);
	sfRectangleShape_setFillColor(aiPaddle.shape, AiFillColor);

	// Setup paddle positions
	playerPaddle.position.x = PADDLE_WIDTH / 2.0f + 10.0f;
	playerPaddle.position.y = (float)(WindowSize.y) / 2.0f;

	aiPaddle.position.x = (float)(WindowSize.x) - 10 - (PADDLE_WIDTH / 2.0f);
	aiPaddle.position.y = (float)(WindowSize.y) / 2.0f;

	sfRectangleShape_setPosition(playerPaddle.shape, playerPaddle.position);
	sfRectangleShape_setPosition(aiPaddle.shape, aiPaddle.position);
	return true;
}

bool setup_ball()
{
	memset(&gameBall, 0, sizeof(ball));
	gameBall.shape = sfCircleShape_create();
	assert(gameBall.shape);
	sfCircleShape_setRadius(gameBall.shape, BALL_RADIUS);

	vec2f origin = { BALL_RADIUS, BALL_RADIUS };
	sfCircleShape_setOrigin(gameBall.shape, origin);

	const sfView* v = sfRenderWindow_getView(renderWindow);
	gameBall.position = sfView_getCenter(v);

	sfCircleShape_setPosition(gameBall.shape, gameBall.position);

	return true;
}

bool setup_score_text()
{
	font = sfFont_createFromFile("res/font/VCR_OSD_MONO_1.001.ttf");
	assert(font);
	if (!font)
	{
		log_err("Font failed to load");
		return false;
	}

	scoreTextPlayer = sfText_create();
	assert(scoreTextPlayer);
	if (!scoreTextPlayer)
	{
		log_err("Unable to create player score text");
		return false;
	}

	sfText_setFont(scoreTextPlayer, font);

	scoreTextAi = sfText_create();
	assert(scoreTextAi);
	if (!scoreTextAi)
	{
		log_err("Unable to create ai score text");
		return false;
	}
	sfText_setFont(scoreTextAi, font);

	return true;
}

void generate_random_direction()
{
	int32 randX = (rand() % 100) + 1;
	int32 randY = (rand() % 100) + 1;

	if (randX <= 50)
	{
		gameBall.direction.x = 1.0f;
	}
	else
	{
		gameBall.direction.x = -1.0f;
	}

	if (randY <= 50)
	{
		gameBall.direction.y = 1.0f;
	}
	else
	{
		gameBall.direction.y = -1.0f;
	}

	gameBall.direction = normalize_vec2f(&gameBall.direction);
}

bool setup_dividing_line()
{
	dividingLine = sfRectangleShape_create();
	assert(dividingLine);
	if (!dividingLine)
	{
		log_err("Unable to create dividing line");
		return false;
	}

	const vec2f DividingLineSize = { 3.0f, (float)(sfRenderWindow_getSize(renderWindow).y) };
	vec2f pos;
	pos.x = ((float)(sfRenderWindow_getSize(renderWindow).x) * 0.5f) - DividingLineSize.x / 2.0f;
	pos.y = 0.0f;

	sfRectangleShape_setSize(dividingLine, DividingLineSize);
	sfRectangleShape_setPosition(dividingLine, pos);

	return true;
}

bool setup_fail_screen()
{
	failScreenText = sfText_create();
	assert(failScreenText);
	if (!failScreenText)
	{
		log_err("Failed to create fail screen text");
		return false;
	}

	sfText_setFont(failScreenText, font);
	sfText_setString(failScreenText, "     YOU LOST!\n\n\n[Space] to go to Menu");
	sfText_setFillColor(failScreenText, sfRed);
	sfText_setStyle(failScreenText, sfTextBold);
	sfText_setCharacterSize(failScreenText, 32);

	sfFloatRect bounds = sfText_getGlobalBounds(failScreenText);
	vec2f centre = { (float)(VIEW_WIDTH), (float)(VIEW_HEIGHT) };
	centre = multiply_vec2f_float(&centre, 0.5f);
	centre.x -= bounds.width / 2.0f;
	centre.y -= bounds.height / 2.0f;
	sfText_setPosition(failScreenText, centre);
	return true;
}

bool setup_win_screen()
{
	winScreenText = sfText_create();
	assert(winScreenText);
	if (!winScreenText)
	{
		log_err("Failed to create win screen text");
		return false;
	}

	sfText_setFont(winScreenText, font);
	sfText_setString(winScreenText, "      YOU WON!\n\n\n[Space] to go to Menu");
	sfText_setFillColor(winScreenText, sfGreen);
	sfText_setStyle(winScreenText, sfTextBold);
	sfText_setCharacterSize(winScreenText, 32);

	sfFloatRect bounds = sfText_getGlobalBounds(winScreenText);
	vec2f centre = { (float)(VIEW_WIDTH), (float)(VIEW_HEIGHT) };
	centre = multiply_vec2f_float(&centre, 0.5f);
	centre.x -= bounds.width / 2.0f;
	centre.y -= bounds.height / 2.0f;
	sfText_setPosition(winScreenText, centre);
	return true;
}

bool setup_border()
{
	topBorder = sfRectangleShape_create();
	assert(topBorder);
	if (!topBorder)
	{
		log_err("Failed to create top border");
		return false;
	}

	bottomBorder = sfRectangleShape_create();
	assert(bottomBorder);
	if (!bottomBorder)
	{
		log_err("Failed to create bottom border");
		return false;
	}

	leftBorder = sfRectangleShape_create();
	assert(leftBorder);
	if (!leftBorder)
	{
		log_err("Failed to create left border");
		return false;
	}

	rightBorder = sfRectangleShape_create();
	assert(rightBorder);
	if (!rightBorder)
	{
		log_err("Failed to create right border");
		return false;
	}

	// Vertical borders
	vec2f size = { (float)(VIEW_WIDTH), 10.0f };
	sfRectangleShape_setSize(topBorder, size);
	sfRectangleShape_setSize(bottomBorder, size);

	vec2f pos = { 0.0f, -size.y };
	sfRectangleShape_setPosition(topBorder, pos);
	pos.y = (float)(VIEW_HEIGHT);
	sfRectangleShape_setPosition(bottomBorder, pos);

	// Horizontal borders
	size.x = 10.0f;
	size.y = (float)(VIEW_HEIGHT);
	sfRectangleShape_setSize(leftBorder, size);
	sfRectangleShape_setSize(rightBorder, size);

	pos.x = -size.x;
	pos.y = 0.0f;

	sfRectangleShape_setPosition(leftBorder, pos);
	pos.x = (float)(VIEW_WIDTH);
	pos.y = 0.0f;
	sfRectangleShape_setPosition(rightBorder, pos);

	return true;
}

bool load_sounds()
{
	paddleHitSoundBuffer = sfSoundBuffer_createFromFile("res/sfx/paddle_hit.wav");
	assert(paddleHitSoundBuffer);
	if (!paddleHitSoundBuffer)
	{
		log_err("Failed to load paddle hit sound buffer");
		return false;
	}

	paddleHitSfx = sfSound_create();
	assert(paddleHitSfx);
	if (!paddleHitSfx)
	{
		log_err("Failed to create paddle hit sound");
		return false;
	}

	sfSound_setBuffer(paddleHitSfx, paddleHitSoundBuffer);
	return true;
}

void update_score_text(int playerScore, int aiScore)
{
	char str[10];
	memset(str, 0, sizeof(char) * 10);
	// If not 0 we failed to write the string
	if (_itoa_s(playerScore, str, sizeof(char) * 10, 10))
	{
		assert(false);
		return;
	}

	sfText_setString(scoreTextPlayer, str);

	memset(str, 0, sizeof(char) * 10);
	if (_itoa_s(aiScore, str, sizeof(char) * 10, 10))
	{
		assert(false);
		return;
	}

	sfText_setString(scoreTextAi, str);

	const sfFloatRect TextBoundsPlayer = sfText_getGlobalBounds(scoreTextPlayer);
	const sfFloatRect TextBoundsAi = sfText_getGlobalBounds(scoreTextAi);

	const vec2f DividerLeftSide = sfRectangleShape_getPosition(dividingLine);
	float centreX = multiply_vec2f_float(&DividerLeftSide, 0.5f).x;
	centreX -= (TextBoundsPlayer.width / 2.0f);
	vec2f p = { centreX, 10.0f };

	sfText_setPosition(scoreTextPlayer, p);

	float viewWidth = sfView_getSize(sfRenderWindow_getView(renderWindow)).x;
	float DividerRightSide = sfRectangleShape_getPosition(dividingLine).x;
	DividerRightSide += sfRectangleShape_getSize(dividingLine).x;
	float width = (float)(viewWidth)-DividerRightSide;
	width *= 0.5f;

	centreX = DividerRightSide + width;
	centreX -= (TextBoundsAi.width / 2.0f);

	p.x = centreX;
	sfText_setPosition(scoreTextAi, p);
}

void reset_game_ball()
{
	const sfView* v = sfRenderWindow_getView(renderWindow);
	gameBall.position = sfView_getCenter(v);
	sfCircleShape_setPosition(gameBall.shape, gameBall.position);
	generate_random_direction();
}

void update_state_gameplay(float dt)
{
	static int playerScore, aiScore;
	if (isFirstUpdate)
	{
		//TODO pick direction at random 
		generate_random_direction();
		isFirstUpdate = false;
		playerScore = 0;
		aiScore = 0;
	}

	update_score_text(playerScore, aiScore);
	update_player(dt);

	const vec2f moveVector = multiply_vec2f_float(&gameBall.direction, BALL_MOVE_SPEED * dt * speedMultiplier);
	gameBall.position = add_vec2f(&gameBall.position, &moveVector);
	sfCircleShape_move(gameBall.shape, moveVector);

	update_ai_paddle(dt);

	collision_data data;
	if (aabb_vs_circle(leftBorder, gameBall.shape, &data))
	{
		sfCircleShape_move(gameBall.shape, multiply_vec2f_float(&data.normal, data.penetration));
		++aiScore;
		reset_game_ball();
		speedMultiplier += 1.0f;
	}
	if (aabb_vs_circle(rightBorder, gameBall.shape, &data))
	{
		sfCircleShape_move(gameBall.shape, multiply_vec2f_float(&data.normal, data.penetration));
		++playerScore;
		reset_game_ball();
		speedMultiplier += 1.0f;
	}

	// Check collision against paddle
	if (aabb_vs_circle(playerPaddle.shape, gameBall.shape, &data) ||
		aabb_vs_circle(aiPaddle.shape, gameBall.shape, &data))
	{
		sfCircleShape_move(gameBall.shape, multiply_vec2f_float(&data.normal, data.penetration));
		gameBall.direction.x *= -1.0f;
		if (sfSound_getStatus(paddleHitSfx) != sfPlaying)
		{
			sfSound_play(paddleHitSfx);
		}
	}

	// Check collision against top & bottom border
	if (aabb_vs_circle(topBorder, gameBall.shape, &data) ||
		aabb_vs_circle(bottomBorder, gameBall.shape, &data))
	{

		sfCircleShape_move(gameBall.shape, multiply_vec2f_float(&data.normal, data.penetration));
		gameBall.direction.y *= -1.0f;
	}

	log_dbg("Ball dir %f %f", gameBall.direction.x, gameBall.direction.y);

	if (playerScore == MAX_SCORE)
	{
		appState = sWinScreen;
	}

	if (aiScore == MAX_SCORE)
	{
		appState = sFailScreen;
	}
}

void update_state_game_end(float dt)
{
	if (sfKeyboard_isKeyPressed(sfKeySpace))
	{
		appState = sMenu;
		return_to_menu();
	}
}

void draw_state_gameplay()
{
	sfRenderWindow_drawRectangleShape(renderWindow, playerPaddle.shape, NULL);
	sfRenderWindow_drawRectangleShape(renderWindow, aiPaddle.shape, NULL);
	sfRenderWindow_drawRectangleShape(renderWindow, dividingLine, NULL);

	sfRenderWindow_drawCircleShape(renderWindow, gameBall.shape, NULL);

	sfRenderWindow_drawText(renderWindow, scoreTextPlayer, NULL);
	sfRenderWindow_drawText(renderWindow, scoreTextAi, NULL);
}

void draw_state_game_end()
{
	sfRenderWindow_drawRectangleShape(renderWindow, playerPaddle.shape, NULL);
	sfRenderWindow_drawRectangleShape(renderWindow, aiPaddle.shape, NULL);

	if (appState == sWinScreen)
	{
		sfRenderWindow_drawText(renderWindow, winScreenText, NULL);
	}
	else
	{
		sfRenderWindow_drawText(renderWindow, failScreenText, NULL);
	}
}

void reset_game()
{
	isFirstUpdate = true;
	playerPaddle.position.x = PADDLE_WIDTH / 2.0f + 10.0f;
	playerPaddle.position.y = (float)(VIEW_HEIGHT) / 2.0f;

	aiPaddle.position.x = (float)(VIEW_WIDTH)-10 - (PADDLE_WIDTH / 2.0f);
	aiPaddle.position.y = (float)(VIEW_HEIGHT) / 2.0f;

	sfRectangleShape_setPosition(playerPaddle.shape, playerPaddle.position);
	sfRectangleShape_setPosition(aiPaddle.shape, aiPaddle.position);
}
