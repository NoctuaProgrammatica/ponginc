#include "vector_utils.h"
#include <assert.h>
#include <string.h>
#include <math.h>

#include "math_utils.h"

float get_length_vec2f(vec2f* vec)
{
	assert(vec);
	return sqrtf((vec->x * vec->x) + (vec->y * vec->y));
}

float get_square_length_vec2f(vec2f* vec)
{
	assert(vec);
	return (vec->x * vec->x) + (vec->y * vec->y);
}

vec2f normalize_vec2f(vec2f* vec)
{
	assert(vec);
	vec2f output;

	const float mag = get_length_vec2f(vec);
	if (mag == 0.0f)
	{
		return *vec;
	}

	output = divide_vec2f_float(vec, mag);
	return output;
}

vec2f multiply_vec2f_float(vec2f* vec, float rhs)
{
	assert(vec);
	vec2f out;
	memcpy_s(&out, sizeof(vec2f), vec, sizeof(vec2f));
	out.x *= rhs;
	out.y *= rhs;
	return out;
}

vec2f divide_vec2f_float(vec2f* vec, float rhs)
{
	assert(vec);
	vec2f out;
	memcpy_s(&out, sizeof(vec2f), vec, sizeof(vec2f));

	out.x /= rhs;
	out.y /= rhs;
	return out;
}

vec2f add_vec2f(vec2f* lhs, vec2f* vecB)
{
	assert(lhs);
	assert(vecB);
	vec2f out;
	memcpy_s(&out, sizeof(vec2f), lhs, sizeof(vec2f));

	out.x += vecB->x;
	out.y += vecB->y;
	return out;
}

vec2f sub_vec2f(vec2f* lhs, vec2f* rhs)
{
	assert(lhs);
	assert(rhs);

	vec2f out;
	memcpy_s(&out, sizeof(vec2f), lhs, sizeof(vec2f));

	out.x -= rhs->x;
	out.y -= rhs->y;
	return out;
}

float dot_prod_vec2f(vec2f* lhs, vec2f* rhs)
{
	assert(lhs);
	assert(rhs);

	return lhs->x * rhs->x + lhs->y * rhs->y;
}

vec2f rotate_vec2f(vec2f* vec, float theta)
{
	assert(vec);
	vec2f output;
	const float thetaInRadians = degrees_to_radians(theta);

	output.x = ((vec->x * cosf(thetaInRadians)) - (vec->y * sinf(thetaInRadians)));
	output.y = ((vec->x * sinf(thetaInRadians)) + (vec->y * cosf(thetaInRadians)));
	return output;
}

float get_angle_vec2f(vec2f* vec)
{
	assert(vec);
	return radians_to_degrees(atan2f(vec->y, vec->x));
}

float get_angle_between_vec2fs(vec2f* vecA, vec2f* vecB)
{
	assert(vecA);
	assert(vecB);

	const float dotProd = dot_prod_vec2f(vecA, vecB);
	//acos(DP / (GetLength(VectorA) * GetLength(VectorB)))
	const float angle = acosf(dotProd / get_length_vec2f(vecA) * get_length_vec2f(vecB));
	return radians_to_degrees(angle);
}
