#ifndef TYPES_H
#define TYPES_H
#include <SFML\System\Vector2.h>

typedef signed char int8; 
typedef unsigned char uint8; 

typedef signed short int16;
typedef unsigned short uint16;

typedef signed int int32;
typedef unsigned int uint32; 

typedef signed long int int64; 
typedef unsigned long int uint64; 

typedef sfVector2f vec2f;
typedef sfVector2i vec2i;
typedef sfVector2u vec2u;

#endif // !TYPES_H

