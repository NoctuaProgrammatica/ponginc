#ifndef COLLISION_TESTS_H
#define COLLISION_TESTS_H 

#include <SFML\Graphics.h>
#include <stdbool.h>

#include "vector_utils.h"

typedef struct
{
	vec2f normal;
	float penetration;
} collision_data;


bool aabb_vs_aabb(sfRectangleShape* rectA, sfRectangleShape* rectB, collision_data* result);
bool circle_vs_circle(sfCircleShape* circleA, sfCircleShape* circleB, collision_data* result);
bool aabb_vs_circle(sfRectangleShape* rect, sfCircleShape* circle, collision_data* result);

#endif // !COLLISION_TESTS_H
