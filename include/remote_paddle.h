#ifndef REMOTE_PADDLE_H
#define REMOTE_PADDLE_H

#include <stdbool.h>

void update_remote_paddle(float dt);

#endif // !REMOTE_PADDLE_H
