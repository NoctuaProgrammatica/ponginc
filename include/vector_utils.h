#ifndef VECTOR_UTILS_H
#define VECTOR_UTILS_H

#include <SFML\System\Vector2.h>
#include "types.h"

/// <summary>
/// Returns the magnitude of the vector provided
/// </summary>
/// <param name="vec">Vector to get legnth of</param>
/// <returns>Magnitude/Length of vector</returns>
float get_length_vec2f(vec2f* vec);

/// <summary>
/// Returns the square magnitude of the vector provided
/// </summary>
/// <param name="vec">Vector to get square legnth of</param>
/// <returns>Square magnitude/Length of vector</returns>
float get_square_length_vec2f(vec2f* vec);

vec2f normalize_vec2f(vec2f* vec);

vec2f multiply_vec2f_float(vec2f* vec, float rhs);
vec2f divide_vec2f_float(vec2f* vec, float rhs);

/// <summary>
/// Add to vec2f's together and return the product
/// </summary>
/// <param name="lhs">Left hand side of expression</param>
/// <param name="rhs">Right hand side of expression</param>
/// <returns>vec2f result of addition</returns>
vec2f add_vec2f(vec2f* lhs, vec2f* rhs);

/// <summary>
/// Subtract to a vec2f's from a vec2f and return the product
/// </summary>
/// <param name="lhs">Left hand side of expression</param>
/// <param name="rhs">Right hand side of expression</param>
/// <returns>vec2f result of subtraction</returns>
vec2f sub_vec2f(vec2f* lhs, vec2f* rhs);

/// <summary>
/// Return the dot product of two vectors
/// </summary>
/// <param name="lhs">Pointer to vector on the left hand side of dot product expression</param>
/// <param name="rhs">Pointer to vector on the right hand side of dot product expression</param>
/// <returns>Dot product</returns>
float dot_prod_vec2f(vec2f* lhs, vec2f* rhs);

/// <summary>
/// Return a vector which is rotated clockwise theta degrees.
/// </summary>
/// <param name="vec">Vector to rotate</param>
/// <param name="theta">Rotate angle in degrees</param>
/// <returns>Rotated form of vec</returns>
vec2f rotate_vec2f(vec2f* vec, float theta);

/// <summary>
/// Get the direction of the vector as an angle in degrees
/// </summary>
/// <param name="vec">Vector to get angle</param>
/// <returns>Angle in degrees</returns>
float get_angle_vec2f(vec2f* vec);

float get_angle_between_vec2fs(vec2f* vecA, vec2f* vecB);

#endif //!VECTOR_UTILS_H