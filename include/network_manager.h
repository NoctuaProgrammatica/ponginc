#ifndef NETWORK_MANAGER_H
#define NETWORK_MANAGER_H

#include "types.h"

#include <SFML\Network.h>
#include <SFML\System\Thread.h>
#include <stdbool.h>

typedef struct
{
	uint16 tcpPort;
	uint16 udpPort;
	uint64 disconnectTimer;
} HostSetupInfo;

typedef struct
{
	uint16 tcpPort;
	uint16 udpPort;
	sfIpAddress ipAddr;
} ClientSetupInfo;

typedef struct
{
	sfIpAddress ipAddr;
	uint16 port;
	uint64 lastContactTime;
} HostConnectedClient;

typedef struct
{
	sfThread* thread; 
	sfMutex* mutex; 
	sfTcpListener* listener; 
	sfTcpSocket* socket; 
	sfPacket* packet;
	HostConnectedClient connectedClient;
} HostNetworkData;

typedef struct
{
	sfThread* thread; 
	sfMutex* mutex; 
	sfTcpSocket* socket; 
	sfPacket* packet;
} ClientNetworkData;


typedef enum 
{
	ngsEstablish, 
	ngsGameSetup, 
	ngsActiveGameplay
} NetworkGameStage;

// Host functions 
bool start_host(HostSetupInfo setup);
void terminate_host();
bool is_host_running();

static void run_host(void* data);
static bool wait_for_client_connection();

// Client Functions
bool start_client(ClientSetupInfo);
void terminate_client();
bool is_client_running();

static void run_client(void* data);
static bool establish_connection_to_host();
static bool wait_for_game_setup();

// Utility Functions
static bool send_packet(sfTcpSocket* socket, sfPacket* packet);

#endif // !GAME_HOST_H
